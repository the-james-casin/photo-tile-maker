Install backend dependencies

- `composer install`
- `cp .env.example .env`

Setup storage, key and ui scaffolding

- `php artisan key:generate`
- `php artisan storage:link`

Install frontend dependencies and compile assets

- `npm install`
- `npm run dev`

Run server

- `php artisan serve`
