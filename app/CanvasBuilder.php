<?php

namespace App;

use App\Tile;

 class CanvasBuilder
 {
    /** @var Resource $canvas */
    protected $canvas;

    /**
     * CanvasBuilder constructor
     *
     * @param int $width
     * @param int $height
     */
    public function __construct($width, $height)
    {
        $this->canvas = imagecreatetruecolor($width, $height);

        imagesavealpha($this->canvas, true);
        $color = imagecolorallocatealpha($this->canvas, 0, 0, 0, 127);
        imagefill($this->canvas, 0, 0, $color);
    }

    /**
     * Add a Tile object to canvas
     *
     * @param Tile $tile
     */
    public function addTile(Tile $tile)
    {
        $file = $tile->getFile();

        $blob = file_get_contents($file);
        $image = imagecreatefromstring($blob);

        $image = imagescale($image, $tile->getWidth(), $tile->getHeight());

        imagecopymerge($this->canvas, $image, $tile->getPosX(), $tile->getPosY(), 0, 0, $tile->getWidth(), $tile->getHeight(), 75);
    }

    /**
     * Write canvas object to specified output file
     *
     * @param  string $output
     *
     * @return string
     */
    public function build($output)
    {
        imagepng($this->canvas, $output);

        return $output;
    }
 }
