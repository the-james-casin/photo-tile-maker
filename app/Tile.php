<?php

namespace App;

 class Tile
 {
    /** @var array $attributes */
    protected $attributes = [];

    /**
     * Tile constructor
     *
     * @param array $data
     */
    public function __construct($data)
    {
        $defaults = ['x' => 0, 'y' => 0, 'width' => 0, 'height' => 0, 'image' => null];
        $this->attributes = array_merge($defaults, $data);
    }

    /**
     * Get X position
     *
     * @return int
     */
    public function getPosX()
    {
        return $this->attributes['x'];
    }

    /**
     * Get Y position
     *
     * @return int
     */
    public function getPosY()
    {
        return $this->attributes['y'];
    }

    /**
     * Get width
     *
     * @return int
     */
    public function getWidth()
    {
        return $this->attributes['width'];
    }

    /**
     * Get height
     *
     * @return int
     */
    public function getHeight()
    {
        return $this->attributes['height'];
    }

    /**
     * Get tile file image
     *
     * @return string
     */
    public function getFile()
    {
        $filename = pathinfo($this->attributes['image'], PATHINFO_BASENAME);
        return public_path("storage/images/{$filename}");
    }
 }
