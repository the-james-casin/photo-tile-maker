<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Psr\Log\LoggerInterface;
use Illuminate\Http\Request;
use App\CanvasBuilder;
use App\Tile;

class ImageController extends BaseController
{
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function upload(Request $request) {
        $request->validate([
            'image' => 'required|image'
        ]);

        $image = $request->file('image')->store('images', 'public');
        $image = asset("storage/$image");

        return response()->json(['data' => ['image' => $image]]);
    }

    public function generate(Request $request)
    {
        $content = $request->getContent();
        $response = json_decode($content, true);
        $tileData = $response['tiles'];
        $canvasData = $response['canvas'];

        $canvas = new CanvasBuilder($canvasData['width'], $canvasData['height']);

        foreach ($tileData as $tile) {
            $this->logger->debug($tile);
            $tile = new Tile($tile);
            $canvas->addTile($tile);
        }

        $random = \Str::random(40);
        $file = "storage/$random.png";
        $path = public_path($file);
        $url = asset($file);

        $canvas->build($path);
        return response()->json(['data' => ['image' => $url]]);
    }
}
